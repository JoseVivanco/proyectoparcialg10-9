/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import interfaz.Sistema;
import interfaz.Utilidades;
import java.util.*;

/**
 *
 * @author Brank
 */
public class Promocion {

    private static Integer codigoIncremental = 1;
    private final Integer codigo;
    private String descripcion;
    private Integer diasValidez;
    private String fechaInicio;
    private String fechaFin;
    private Establecimiento establecimiento;
    private Tarjeta tarjeta;

    public Promocion(String descripcion, Integer diasValidez, String fechaInicio, String fechaFin, Establecimiento establecimiento, Tarjeta tarjeta) {
        this.codigo = codigoIncremental++;
        this.descripcion = descripcion;
        this.diasValidez = diasValidez;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.establecimiento = establecimiento;
        this.tarjeta = tarjeta;
    }

    public static void registrarPromocion(Empresa empresa, Establecimiento establecimiento) {
        System.out.println("Ingrese descripción de la promoción :");
        String nDescripcion = Utilidades.ingresoString();
        System.out.println("Ingrese dias de validez :");
        String d = Utilidades.ingresoString();
        while (!(Utilidades.isNumeric(d))) {
            System.out.print("Por favor, ingrese un número : ");
            d = Utilidades.ingresoString();
        }
        Integer nDias = Integer.parseInt(d);
        System.out.println("Ingrese fecha de inicio de la promoción :");
        String nFechaInicio = Utilidades.ingresoString();
        System.out.println("Ingrese fecha de expiración de la promoción :");
        String nFechaFin = Utilidades.ingresoString();
        Tarjeta nTarjeta = Tarjeta.nuevaTarjeta();
        Promocion nPromo = new Promocion(nDescripcion, nDias, nFechaInicio, nFechaFin, establecimiento, nTarjeta);
        establecimiento.getPromociones().add(nPromo);
        System.out.println("Promoción agregada con éxito");
    }

    /* GETTERS & SETTERS */
    public Integer getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getDiasValidez() {
        return diasValidez;
    }

    public void setDiasValidez(Integer diasValidez) {
        this.diasValidez = diasValidez;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }
}
